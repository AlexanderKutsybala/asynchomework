package com.alexander.asynchome;

import android.os.Handler;
import android.os.Looper;

public class MyObservable<T> {

    private static Callabale callable;
    private static Looper looper;
    private static Looper mainLooper;
    private static Callback callback;


    public MyObservable(Callabale callable){

        this.callable = callable;
    }
    public MyObservable(Callabale callable, Looper looper){

        this.callable = callable;
        this.looper = looper;

    }
    public MyObservable(Callabale callable, Looper looper, Looper mainLooper){

        this.callable = callable;
        this.looper = looper;
        this.mainLooper = mainLooper;
    }


    public static <T> MyObservable<T> from(Callabale callable) {

        return new MyObservable(callable);
    }

    public static <T> MyObservable subscribeOn(Looper looper){

        return new MyObservable(callable,looper);

    }

    public static <T> MyObservable observeOn(Looper mainLooper){


        return new MyObservable(callable, looper, mainLooper);

    }

    public static void subscribe(Callback callback){

        MyObservable.callback=callback;

        //Если запущено из потока без looperа, бросается исключение
        if (Looper.myLooper() == null) {
            throw new RuntimeException(
                    Thread.currentThread()
                            + " that has not called Looper.prepare()");
        }

        if (looper==null){
            looper=Looper.myLooper();
        }

        if (mainLooper==null){

            mainLooper=looper;
        }

        new Handler(looper).post(() -> callable.func("callable"));

        new Handler(mainLooper).post(() -> callback.func("callback"));



    }
}
