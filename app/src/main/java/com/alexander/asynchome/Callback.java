package com.alexander.asynchome;

public interface Callback<T> {

    T func(T t);
}
