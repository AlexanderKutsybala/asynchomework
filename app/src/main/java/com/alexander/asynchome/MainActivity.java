package com.alexander.asynchome;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Callabale callable = new Able();
        Callback callback =new Back();

        MyThread myThread =new MyThread("MyThread");
        myThread.start();

       /* MyThreadD myThreadD = new MyThreadD("MyHand");
        myThreadD.start();
        myThreadD.interrupt();
       */


        MyObservable.from(callable)
                .subscribeOn(myThread.getLooper())
                .observeOn(Looper.getMainLooper())
                .subscribe(callback);



    }

}

/*
 //класс с лупером для проверки

class MyThreadD extends Thread {
    public Handler handler;
    public MyThreadD(String name){
        super(name);
    }
    public void run() {
        Looper.prepare();
        handler = new Handler() {
            public void handleMessage(Message msg) {
// process incoming messages here
            }
        };
        MyThread myThread = new MyThread("MyThread");
        myThread.start();

        MyObservable.from(new Able())
              //  .subscribeOn(myThread.getLooper())
                .observeOn(Looper.myLooper())
                .subscribeOn(myThread.getLooper())
                .subscribe(new Back());

        Looper.loop();
    }
}
*/



